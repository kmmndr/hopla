#!/bin/sh

set -e

temp_folder="$(mktemp -d)"
git clone https://gitlab.com/kmmndr/hopla.git $temp_folder

old=$(pwd)

cd "$temp_folder"
./install.sh

cd "$old"
rm -rf "$temp_folder"
