# Hopla deploy tool

## Installation

Use the quick installer, to install or update

```
curl https://gitlab.com/kmmndr/hopla/raw/master/dl.sh | PREFIX=~/.local bash
```

Or git clone the repo and help yourself

```
git clone https://gitlab.com/kmmndr/hopla.git
```

## Usage

```
Usage: hopla [options] [COMMAND] [args]
Commands:
  hopla                Display this menu
  hopla usage

  hopla init           Create init directory
  hopla list           List available roles/tasks/libs

  hopla run <target> (<target> ...)
                       Run targets locally
  hopla compile <target> (<target> ...)
                       Create targets batch script
  hopla push <remote> <target> (<target> ...)
                       Push batch script on remote
  hopla push inventories/<inventory> <target> (<target> ...)
                       Push batch script on remotes from inventory

Options:
  -p, --path <path>    Set rules source directory (default to ~/hopla)
  -V, --version        Output current version of hopla
  -h, --help           Display help information
```

### First start

You need to create `hopla` template folders, you can do it by youself or simply
run `hopla init`.

This will create the following structure in your home (or
anywhere else if you set path using `-p` option)

```
~/hopla
├── roles
├── inventories
├── libs
└── tasks
```

Then create your own `tasks` files as batch shell script, group them amongs
`roles` add custom `libs` if you want and describe your hosts in the
`inventories`

### Quick start

```
HOPLA=~/hopla

hopla init

echo "date > /tmp/date"                 >  $HOPLA/tasks/print_date
echo "cat /proc/cpuinfo > /tmp/cpuinfo" >  $HOPLA/tasks/print_cpuinfo
echo "tasks/print_date"                 >  $HOPLA/roles/print_all
echo "tasks/print_cpuinfo"              >> $HOPLA/roles/print_all
echo "127.0.0.1"                        >  $HOPLA/inventories/local

hopla list

hopla push inventories/local roles/print_all
```
