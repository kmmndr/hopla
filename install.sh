#!/bin/sh
# Usage: PREFIX=/usr/local ./install.sh

set -e

if [ -z "${PREFIX}" ]; then
  PREFIX="/usr/local"
fi

BIN_PATH="${PREFIX}/bin"
mkdir -p "$BIN_PATH"

install -b -p -m 0755 hopla "$BIN_PATH"
